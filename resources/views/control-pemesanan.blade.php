
@extends('master-admin')

@section('website-title', 'Admin Pemesanan')

@section('judul_halaman', 'Pemesanan')

@section('data-table')
        <table id="example" class="table table-striped table-bordered mb-9" style="width:100%">
          <thead>
              <tr>
                  <th>id_produk</th>
                  <th>Email Pemesan</th>
                  <th>Nama Produk</th>
                  <th>Display Produk</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td class="align-middle">1</td>
                  <td class="align-middle">samodra@gmail.com</td>
                  <td class="align-middle">Paket Advance</td>
                  <td>
                      <img src="/assets/paket-advance.png"/>
                  </td>
                  <td class="align-middle text-center">
                      <a data-toggle="modal" data-target="#modalDelete" class="btn btn-danger">Delete</a>
                  </td>
              </tr>
          </tbody>
          <tfoot>
              <tr>
                     <th>id_produk</th>
                  <th>Email Pemesan</th>
                  <th>Nama Produk</th>
                  <th>Display Produk</th>
                  <th>Action</th>
              </tr>
          </tfoot>
       </table>
@endsection
@section('modal-delete')
<div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
           
          <div class="modal-body">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Hic reiciendis repellat aliquid aut facilis illum aspernatur aliquam ullam incidunt vitae.
          </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-danger">Delete</button>
              </div>
 
          </div>
      </div>
  </div>
@endsection
