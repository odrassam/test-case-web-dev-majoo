<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Login</title>
    <style>
        .login-container{
          display: grid;
          justify-content: center;
          align-content: center;
          width: 100%;
          height: 100vh;
          background-color: #f4f4f4;
        }
    </style>
  </head>
  <body>
    <div class="login-container">
        @if(\Session::has('alert'))
          <div class="alert alert-danger">
            <div>{{Session::get('alert')}}</div>
          </div>
        @endif
        @if(\Session::has('alert-success'))
          <div class="alert alert-success">
              <div>{{Session::get('alert-success')}}</div>
          </div>
          @endif
          <div class="card" style="width: 30rem; height: 20rem;">
            <div class="card-header">Login for Admin</div>
             <form action="{{ url('loginPost') }}" method="post">
              {{ csrf_field() }}
              <div class="modal-body">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Username</label>
                          <input name="username" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                      </div>
                      <div class="form-group">
                          <label for="myPassword">Password</label>
                          <input name="password" type="password" class="form-control" id="myPassword" aria-describedby="emailHelp">
                      </div>
              </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </form>
          </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script>
       
    </script>
  </body>
</html>