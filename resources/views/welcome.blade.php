<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>List Products</title>
    <style>
        .prodcut-wrapper{
            display: grid;
            width: 100%;
            grid-template-columns: repeat(auto-fit, minmax(250px, 300px));
            grid-row-gap: 12px;
            justify-content: center;
        }
        body{
            overflow-x: hidden;
        }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-dark bg-dark">
        <span class="navbar-brand mb-0 h1">Majoo Teknologi Indonesia</span>
        <a href="/login" class="btn btn-primary">Jual Produk +</a>
    </nav>
    <div class="mt-3 ml-4">
        <h2>Product</h2>
        <div class="prodcut-wrapper mt-4">
            <div class="card" style="width: 18rem;">
                <img src="/assets/paket-advance.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Paket Advance</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <div class="text-center">
                        <a href="#"  data-toggle="modal" data-target="#exampleModal" class="btn btn-primary text-center">Beli</a>
                    </div>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img src="/assets/paket-desktop.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Paket Desktop</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <div class="text-center">
                        <a href="#"  data-toggle="modal" data-target="#exampleModal" class="btn btn-primary text-center">Beli</a>
                    </div>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img src="/assets/paket-lifestyle.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Paket Lifestyle</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <div class="text-center">
                        <a href="#"  data-toggle="modal" data-target="#exampleModal" class="btn btn-primary text-center">Beli</a>
                    </div>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img src="/assets/standard_repo.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Standard Repo</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <div class="text-center">
                        <a href="#"  data-toggle="modal" data-target="#exampleModal" class="btn btn-primary text-center">Beli</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <form>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Kamu</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Beli</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script>
       
    </script>
  </body>
</html>