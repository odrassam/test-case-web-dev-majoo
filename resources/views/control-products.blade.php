
@extends('master-admin')

@section('website-title', 'Admin Products')

@section('judul_halaman', 'Products')
@section('data-table')
                <table id="products" class="table table-striped table-bordered mb-9" style="width:100%">
          <thead>
              <tr>
                  <th>id_produk</th>
                  <th>Nama Produk</th>
                  <th>Display Produk</th>
                  <th>Harga</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td class="align-middle">1</td>
                  <td class="align-middle">Paket Advance</td>
                  <td>
                      <img src="/assets/paket-advance.png"/>
                  </td>
                  <td class="align-middle">Rp.100.000</td>
                  <td class="align-middle text-center">
                      <a data-toggle="modal" data-target="#exampleModal" class="btn btn-warning">Edit</a>
                      <a data-toggle="modal" data-target="#modalDelete" class="btn btn-danger">Delete</a>
                  </td>
              </tr>
              <tr>
                  <td class="align-middle">2</td>
                  <td class="align-middle">Paket Desktop</td>
                  <td>
                      <img src="/assets/paket-desktop.png"/>
                  </td>
                  <td class="align-middle">Rp.100.000</td>
                  <td class="align-middle text-center">
                      <a data-toggle="modal" data-target="#exampleModal" class="btn btn-warning">Edit</a>
                      <a data-toggle="modal" data-target="#modalDelete" class="btn btn-danger">Delete</a>
                  </td>
              </tr>
              <tr>
                  <td class="align-middle">3</td>
                  <td class="align-middle">Paket Lifestyle</td>
                  <td>
                      <img src="/assets/paket-lifestyle.png"/>
                  </td>
                  <td class="align-middle">Rp.100.000</td>
                  <td class="align-middle text-center">
                      <a data-toggle="modal" data-target="#exampleModal" class="btn btn-warning">Edit</a>
                      <a data-toggle="modal" data-target="#modalDelete" class="btn btn-danger">Delete</a>
                  </td>
              </tr>
              <tr>
                  <td class="align-middle">4</td>
                  <td class="align-middle">Standard Repo</td>
                  <td>
                      <img src="/assets/standard_repo.png"/>
                  </td>
                  <td class="align-middle">Rp.100.000</td>
                  <td class="align-middle text-center">
                      <a data-toggle="modal" data-target="#exampleModal" class="btn btn-warning">Edit</a>
                      <a data-toggle="modal" data-target="#modalDelete" class="btn btn-danger">Delete</a>
                  </td>
              </tr>
          </tbody>
          <tfoot>
              <tr>
                  <th>id_produk</th>
                  <th>Nama Produk</th>
                  <th>Display Produk</th>
                  <th>Harga</th>
                  <th>Action</th>
              </tr>
          </tfoot>
       </table>
@endsection
@section('modal-delete')
<div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
           
          <div class="modal-body">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Hic reiciendis repellat aliquid aut facilis illum aspernatur aliquam ullam incidunt vitae.
          </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-danger">Delete</button>
              </div>
 
          </div>
      </div>
  </div>
@endsection
@section('modal-add-update')
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
              <form>
          <div class="modal-body">
              <div class="form-group">
                  <label for="nama-product">Nama Product</label>
                  <input type="text" class="form-control" id="nama-product" aria-describedby="emailHelp">
              </div>
              <div class="form-group">
                  <label for="harga Produk">Harga Product</label>
                  <input type="email" class="form-control" id="harga Produk" aria-describedby="emailHelp">
              </div>
                  <input type="file"  id="exampleInputEmail1" aria-describedby="emailHelp">
          </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary">Update</button>
              </div>
          </form>
          </div>
      </div>
  </div>
@endsection
