<?php

namespace App\Http\Controllers;

use App\ModelAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function products(){
        if(!Session::get('login')){
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data = DB::table('product')->get();
            return view('control-products', ['data' => $data]);
        }
    }
    public function pemesanan(){
        if(!Session::get('login')){
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
        return view('control-pemesanan');
        }
    }
    public function login(){
        if(Session::get('login')){
            return redirect('admin/products');
        } else {
            return view('login');
        }
    }
    public function doLogin(Request $request){
        $username = $request->username;
        $password = $request->password;
        $data = ModelAdmin::where('username', $username)->first();
        if($data){
            if(Hash::check($password, $data->password)){
                Session::put('username', $data->username);
                Session::put('login', TRUE);
                return redirect('admin/products');
            } else {
                return redirect('login')->with('alert', 'Password atau username salah !');
            }
        } else {
                return redirect('login')->with('alert', 'Password atau username salah !');
        }
    }
    public function logout(){
        Session::flush();
        return redirect('/')->with('alert', 'Kamu sudah logout');
    }
}
